;; By James McClain (james.mcclain@gmail.com)
;; Based on http://dishevelled.net/elisp/lambda-mode.el and http://www.emacswiki.org/emacs-de/PrettyLambda

(defvar big-lambda-image
  (create-image
   (base64-decode-string
"iVBORw0KGgoAAAANSUhEUgAAACEAAAAwCAYAAACfbhNRAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A
/wD/oL2nkwAAAAlwSFlzAAAN1wAADdcBQiibeAAAAAd0SU1FB90FFQA2Hr5AIZoAAAjKSURBVFjD
vVh7bFPXGf9958axE8fB6cibp0NE1UeWlvEca6sOUgIpaGGDqhUTnWi0tiJZU63StI49WmlaGdAx
SHkVBstWyksbW0soqC0VKK/CmpRZIYSkSbTQFOwksxPb1/d+3/7Y9eqlUF6mR7Js+Zx77u/8vt/5
XkDcYObY9xwROSH/Pz5k5kUAICIQESR0iEg8gOdERAzDiLa0tGzevHnzytOnT78aiUQGRUSYuSYe
cCIGjQLzbQDHg8Hg+aqqqipN01RmZqZ9aGgo6vf79Y0bN744duzYuSLyI6XU7xIKQkSIiEREAqZp
SmVl5eO5ubmpuq4zABARbDabam9v//eWLVtqUlNTJxMRJQqEEhEQkTDzEwDSjh49+nIoFDKj0Si7
3W6H2+12EBF1dXUFx40bl/r666//wjJHTUL1YG3aFQqFPlm6dOn8gYGBM6NEKZcvXz61evXqRysq
Khb19/e/J9aDCROoiIwRETl58uRLhmEMW+9dw8wpzJwmIltERILB4Plly5aV7N+/v9IS6cMJY4OZ
ZxuGIQMDAx9am8+5wpoqERGv17tj7ty5D+q6PigiRxLGBjM/Ho1GxTRNYeaNo68gMyvrZQ0iIitW
rFjQ0dHxJxEJWfOUCHP8xDTNGIgJV/MBzDxBRKSlpaXmzTffXG2ZLZ+ZtVu6Hdb3GBGBUsoQkR6l
1BfEa833APjY4/HMP3To0DlrbolSyrxlECJyWdO0yyJyVtO0L9iYiBDnFmrS0tKmmKYpgUDAS0RP
JUIPo09N1zAdiYg0NjaubW5uXmuZRLsVN65GU09E8mU+xZr3Tpky5aGmpqaz1kEKR4eAm9HE9fn4
z03yV6fTeVdbW5vfYuBBpZR8JSDixrt2uz31s88+i4RCoU4ievxWIqu6ySvdDAClpaWTuru73yGi
Byzb3n4QsVtDREtjJ29pafnY+u35qs2xYWRk5JOGhoZ/Xbx4ccgyQ/FXJkxmLgeQfurUqR1OpzOp
vb19UNf1AQALbjaOqJswxdpIJHKptrb2dHp6erKIwO/3f0hE5bcdhMXCTACe1tbW3W63OxmAZGVl
Odra2k4C+Boz225GnOo6vSpZQLaYphlat27dsYyMDDsAOJ1O24kTJ7zW/AO3RRMWvSQiHgDFXq/3
D/n5+Skxgnw+X3hwcFCPRCIjAB64bcJUSrGIbAGAXbt2HXU6nbaY93z66acfc7lctlAo1BkDcaO6
UNfBgiYiOUQ0/8KFC38mIiilSNd1c8GCBdM8Hs/ysrKyGX19fY0AHrotTBCRycwbAODAgQN/T0tL
s4mIdHZ2BqdNm/YUABQWFs49duzYCUs/4260GrgmEyKSoZR6rK+v760LFy4MEREZhiErV66cZrPZ
7mDmhoyMjDm1tbWfWKAXJowJZoZSCiLyIgAcOnRo//jx49MAoKenJzhnzpwnAASZ+ftKKSxbtmx8
IBDoADA/YSCUUmBmRUTVPp/vVFNTU79hGByJRMx58+ZNcrlcd4vIyzab7TwAFBUVTfT7/R8DmJ1Q
TRDRCwDwxhtvbJ86deoYAAgEAtFHHnnkuxbQ31hLLxYWFhZ1dHScAZCfaGH+OhAItDU1NfVHo1EW
ESQlJVFOTs48AH+MW3c6Ozt7xt69e89YpvzmLYGIKwt/AAB1dXWv5ebmpoiI9Pf3h5599tnvWUur
4h7b53Q6PV6vN2gYRigW6m+ZCSJaOzIy0l1XV3c+JSUlSSlFpmmKx+NZDuBdIhqw+hpJInIYAJ55
5pm7fT5ffUyc11sUqasEqkcB3NHY2LjL5XLZREQuXboUXrVq1bc0TbOJyE/jSgFRSg0B0O+99957
uru7zwC4x9pOu2EQceF6va7rvh07djS43W67pmkqHA6bxcXFTwLoUUo1xLnmWGJ5Ji8v7+v19fWt
FguzrGLqqmnB/4qqK7BwP4Apra2tu7Oysuyx/0tKSgocDkc2gB9fpUR43+12f2PTpk0dpmmGYzfr
St6TiGLlA4jocxBxLGxn5siGDRvesZIW6erqCpSWlv4QABPRvittLiKHk5KS7OXl5Tm9vb2HAXxH
RJKuVEwxs2YB6RGRwXgQiplzANx/7ty52li4ttls2owZM3JcLtddIvKzq6X2Sql6AFiyZMn0hoaG
D6x1pVYXaDQTygI43jAMM1aLQinFAHYBwO7du99yOBxJREQ9PT2BxYsXl1sP//4aqX1XQUHB7PXr
17dGo9HLRLQznv44EFERWU5E0HV9SFmTygrXCzo7O/eGw2FTKUUAMGHCBFdeXl4ZM+8losA1Cpyj
GRkZM4qLi9Pr6+triGgsM6+L2T6+/0lEa4eHh9s1TUuJNc5YRF6xAtXfYqnbyMhItLy8/GHr9C9Y
seTLou6+5OTk1FmzZuXu3Lmzwe/3NxFRNTO/Gl/dM/NBALkfffTRAbvdnqOsSDmGiFZ8+umndV6v
d4CZRUTE5/Ppd95555MAGomol4j4y0Aopd4DgIULFy4HgOrq6p/7fL56Iqqyqvf35b/9yvKenp6/
mKZpAkBSrFNDRDh48ODegoKCdF3XzXA4bFZUVDyolLKJSMXVrtuoih0AtmdnZz+Vm5v7msvlsj3/
/PMvlZSUTJ4+ffpsTdNSDcPY09jYWL9t2zbvkSNHdgFojjVTxe/3N1ZXV/9y4sSJTqtvGdi6det2
h8NhENFkEVFEdM2Kl5knE1FnX1/fW2vWrKmZNGlS2vDwsOHz+cIxPWRlZaXMnDlz0uLFi38rIquV
iFQCwJ49ezZnZmbaBwcH9d7e3mBZWVmhw+EYJyK/sli4JgDrlnWJyOG8vLxFlZWVi86ePTtomibn
5eU58/PzncnJySozMzO1rKzsFWv9JhKRgXA4zM3NzVvtdrsDAHRdjxQVFS1NT08vtPrNsRdcV6Vm
ia+DiAqCweC5t99+e/Px48c7bTYbrVq1av59991XaS2/h4j+CWb+QETqReQfoz/MvPpGU3hmjk8H
nhMRv4iIaZqRWHeYmZuYeUps7/8ApmyI0TsACIIAAAAASUVORK5CYII=")
   'png t))

(defvar big-loop-image
  (create-image
   (base64-decode-string
"iVBORw0KGgoAAAANSUhEUgAAADIAAAAgCAYAAABQISshAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A
/wD/oL2nkwAAAAlwSFlzAAAN1wAADdcBQiibeAAAAAd0SU1FB90FFQECEg0e9HEAAA2wSURBVFjD
rVlrdFRVlt5731vvSlIJqaq8ExJQQXkpb8FGEHFoQhDsAZlxDcNDHjarQQdRwyJqY6M2BgF1WpZ2
aMB2EBgWSENjwMbphkimQUNgCUUSyKPyIJVXpVKVetyz58+9rEsEFHvOr3vr3vPt/Z19zv723QXQ
ZzDzTfdCiFxmfpWZ/8LMfr55NDHzAWZeJISQtfl9MfoOIYT+Oo2ZX2bmE8zcwsyKit3LzB5m3sfM
zwohrNrcW+HjrYggIjCznZn3IOJ0AIBAIHDV6/V+5fF4Lra3t3fn5OSk5eTkDHW73T8zm82J6tzf
EFGhnhARfY8EEQEzxzPzx4j4lIpf29LSUlZTU3MxEAj0pKampmRmZg5LSkoaabFY+qmY2xBxFSIK
zc9bEtGReAwASgEArl+/Xrpnz57d3377rc9ut8sJCQlGWZapt7c35vP5wgaDgaZOnTpw2rRpKywW
Sx4zNwLAA0TUoTmtw0dEZCHEJET8CwBAW1vb/+zbt+8PX3/9dVNf/Pb29rAsyzRhwoSs/Pz8Z+Pi
4oYBQJCZhxJRtYZ3ExEhBBIRCyHmIeKnAKAcOXKkcPfu3d8MGjTIEYvFhJ68SphRXZa6urrA6tWr
84cMGbJcdXoAEVVrZHT4TyHiXmaOnThx4tVdu3adzcvLixdCsIqnEb6Bj4hQXV3dvXLlysdGjRr1
goo/lIgqtcVHfbiFEJMR8YSiKIHi4uKlra2tIavVKmsLqosaqvuUdeHF+vr6wMyZMwcXFBRsUn/L
RcSrmjEhxARE/KuiKMFNmzYtbmtr6+2LfxsbyMzc2dkZGTduXOa8efPeU99LICI/M4P2MgKADQC6
hRDKhg0b5ofDYcVgMJC2+uFwWBFCMACA0WiUJElC9RzcMERE2NzcHJw6derA2bNnb2bmGABYiCjG
zHEA4Gdm8c477zzj9Xp7EhISjBp+NBoVvb29SiwWE9phNhqNZLPZDKrTjIjY0NDQU1BQMDg/P/+3
zHyJiAYxM8jqirIQ4hgiwsGDB9e0tLQE09LSbNXV1f77778/KTc31+l2u/uZTCarEELp6Ojo8Hq9
vjNnzjQ6nU6zLMsaKXC73dbS0tKqlJSUjePHj3+ZmU8CwARmPoSIcOzYscKGhoaAw+EwVVVV+YcM
GZKUlZWVmJ6e7urXr5/TYrEkIaKBmSOBQKC1rq6u/sqVK80VFRVtAwcOjM/MzLTt2bOnctCgQbsG
DBjwjBBiESJ+jCrbUQBQ7vV6j6xfv/49p9NpDofD4sUXX1zhcrkelyRJi3ovAJi1m1AoVF9aWrpl
7969F/Py8uJYCy8iNjU1BYuKip5LS0v7J2b+CBEXt7S0nFi1atXbZrNZevLJJwdPnDhxflxc3IOy
LN84qgBwGQA6ASAdALK0re/z+b7csmXL+8FgUImPjze0trb2bt26dackSQYiitPOSBkijn399dfn
+v3+yD333NNv0aJF2yRJsjDznxCxCBHP6vZwOgD8KwC8CQBQU1PzX+vWrdtx7733xmvbwmAwUH19
fWDbtm27DQaDg5ljhYWFc51Op3XJkiWFdrv9PhVuKwDsYea/E1HkFnLwIDMXIuJsZoajR4+uPXr0
6Hd2u92Qn58/cvz48S8DwAwSQjgRcWxtbe1n165d67bZbPLixYs1EnOIaIZGQhMyRPQi4ltqxqrM
zc2d98YbbyxsbGwMavYjkYgiyzJ9+eWXbwMAVFVVfZqdnZ2wevXqXXa7/T5mfkvNSL9CxNN6EnpR
RcRzRDSHmccDAEyfPv2tyZMnD4hGo+Lo0aPn1Gw6lxDxCQCAkydPnjAajbRixYqlRGRh5l8Q0X/r
VVivCTpSQ5m5on///v+8YMGCid3d3REtRTudTnNpael3L7300pzDhw9/sXTp0t+rjo4hopduVUlo
h18vdqqwliFiJgBAQUHBxlgsJq5evdrd1tZ2EgDyCQDGhMPh3oqKihaXy2Vxu92PAUApEe1jZuyr
zHpjulT5EDNHx40b92JCQoIpGo0qarplu91uiEajyvz58/9NnTqJiMqZGYUQNzl8u6FiASI2MPNT
RGRatmzZTACACxcunAQABwHA8EAgcLGsrKxjzpw5k9S5C4QQN1TzTkTUawURpwAArlix4pd1dXU9
qCkZAIdCIUVRlIhK/Kw6h2+3SHcgIxHRfgC4kpWV9bjb7TaXl5dfAQAgABh4/fr18yaTCdPS0kYA
QA0iNqrPfpQBNSp/ZeY9ycnJE2fNmnVfV1dXRM1imJycbD516tQxdcqavoXjj7VDRIp6+4HFYsl0
Op3WUCgUi8ViXQQA/Zqbm+vS09PNRqPRBQCntVW+29USQjwNAPDEE0+s7OzsjEiSRADARISff/65
JxAIeBBxfd/z9hPGEUQEm81mCIfDSjQa7SQAkNrb2ztsNpuEiDIA1PbdOj9yKLIsMzO/ZrPZ8mbP
nv2AmlEQACA7O9t2/Pjx36nba/0/wgIRParyy5FIRCiKEiI1zKwrII3/iBEiehUA4NFHH11SW1sb
0BKCwWCQjhw5cjkYDNYBQJFqV4b/p0EAwElJSQ6/3x8VQoQAYKBWDd8lgRuplJlftdlsAwsKCu4V
QjAigqIowmQySadPn94OACSEeFOtwe7aaWbOAgCIxWKK2WyWZFm2EwC0O53O1La2tmgoFGoAgMfU
96WfEHLt8rcAAJMnT55fU1PTrT1OTk42l5SU/D0QCFxCxLXM7LqdlvzAmAoAEAwGo2azWTIajckE
ALVut3uQLMtYVVV1BgDsQohJRBT7KSFWPwmCQoitDodj9IgRI5I1XWFmTk9Ptxw8eHCbSuBgX/G7
QxRACKFliEWhUKi1tbU16HK5bERkJgCojI+PH5qRkWH+5JNPvhZCxBBxlzpZuotwa1HRvJoAAGC1
Wo2yLJOWiq1Wq6G0tLSmoaHhECKOZeZfalv5TpFR069g5uEAMK6lpeWryspK/4QJE0ZrZ+ScyWRK
HDp0aLLRaKRLly7tAIAMIUSJloLvJvTqp+zfiOjBa9eu7T19+nRjT09PTPumYGbOycmJ27JlS0k0
Gm0HgG1CiJFExD+0SOooBQDYuXPnZw6HQ+7fv/80AKgkZj5JRDBmzJgHzGazvHnz5gN+v/88Ii4Q
Quzvm4o10L6CpmrJVGb2I+LDjY2NRzZu3PiHXbt2/bmoqOiN6urqblmWSYuYxWKRP/roo/9Q5/6v
EOLnejt6fHVbykKIKwCQfPbs2c0+n693ypQpWXa7PRcA9hIRnWfm9uHDhz/T3NwcSk1Nta5bt26d
z+f7m1o6h5h5iRDCpSelCRoz38/MLzBzMyJ+oSiKpays7M3p06dvLCoqWgEAEBcXN3jlypVT2tra
enVZDi9evOjbuXPnMiFEBBEPCyGOM/NEPb4Qws3MqwEgiogDqqqqPnn33Xe/6Orqiubn589Vfdih
fY8sQ8T/LCsr27h///5yq9Uqezwe//Lly8ePGTNmqdlsTtFEDwAuqNcDAeBGrykUCtV6PJ6DO3bs
OBGJRJRHHnkk5+mnn36PmQ8BwAgAyHz//fcX+Hy+oL6poFYA+Pzzzy91uVzTZFlGna0bZzQYDNaW
lpZuPXTo0HepqanWIUOGpM6dO3cbM/+JiGag7lAHFEVRnnvuubnJyclmo9EoBYPBaE9PT2z06NHp
2dnZKU6nM9VkMjkAgMPhcFdXV1eb1+ttbmpq6jp37tz1pKQkU2JiounChQsdJSUl281mczYzp6rl
d3koFLq2ePHiZQMGDIjTvvO1HkBtbW13Xl5eQl5entPtdveTJEliZu7u7u5uamrqKC8vb0pKSjIZ
DAa6fPmyv6Sk5PdmszkNALIQsR51HY5ZiHigo6OjfNGiRYXDhw9PUhRFaCsXjUaFWqAxIoIkSWgy
mSSTySTpmxDnz5/v+OCDD9akpKQ8zszriejXatR+jYjr2tvbzyxcuLBw2LBhifqiU0t4iAiRSERo
JA0GAxERas2+b775puPDDz8sdLlck5j5BSIqvqmLomabnYj4TFNT059feeWVrZmZmTYiwj7tGrxd
eqyoqOgoLi5+Nicn5xfMfJyIpuo7jkKITxFxns/nO7V8+fLXBg8enIA3iwjfoQPK9fX1Pa+99tqy
zMzMmcx8mIjyVW35fhkihDiJiD/z+/3ni4uLN3R2doaTkpLMWnR0egHMzJIkYSAQiCEirFmzZlVy
cvIjzPwtEY1gZtQ7py7WPkScEwwGq7Zv3/76lStXOtxut1XD79PxZEmSyO/3RwwGA61du/YVh8Px
EDOfIaKxt+z96nupQoiPEXEhAHBFRcV7hw8fPuXxeLoSExONdrvdQEQYCoVizc3Nvf3797fPmDFj
9MiRI1cSkZmZDxDR7B9Q/w2IWAgA4PF4dhw4cOCLysrKdrfbbdbwe3p6Yq2trb0pKSmWWbNmjRk9
evSvENHEzH8kon/p6/OtQkhqk/hhZv4MEdMURYFAIPBdY2PjGa/XWxMKhXozMjKyMjIyHkpMTBwn
yzIwcwci/jsiHtT17L5XfmitU2Z+iJn/iIj3CCGgq6vrvNfrLauqqvKEw+Fobm5uTk5OzliHwzHW
YDAQM3cBwFwiOqb5+KNKDZ3hh5n5bWb+ipmbmTmqCnQXM59n5t8JIabdqgNyO3z9cyHEKGbexMxn
mTmg+8siwswXmbmEmX+uF8pb4f8fsgcWHWgXBiIAAAAASUVORK5CYII=")
   'png t))

(defvar big-function-image
  (create-image
   (base64-decode-string
"iVBORw0KGgoAAAANSUhEUgAAACAAAAA6CAYAAADRN1sJAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A
/wD/oL2nkwAAAAlwSFlzAAZcUgAGXFIBQXmCwgAAAAd0SU1FB90FFQEJH5BbUQcAAAd6SURBVFjD
vVlbbBTnFf7OPzt7NazZXQwb27IJweZiFgvjWgJSuailQqgXkQceIvWBqqBKtKoi96lqUVDT9oFW
PASpJC+paJrmpcjtQ2miWiVKHCeOAbMxsMEG38BZ72LvYq+9l5lz+jJbTS1ovRfzv8zon8v5/nO+
c+b7zwBVHMwMESme+0Xkl8wcE5GCiJjMvMTM10XkF8zsQ7UHMwMAROR7Yg1mFmY2CoWCuWJORKSt
asZFhCwQPxYRyefzswsLC7ctmwsi8htmnhDbYOaZantgv4hILpdL9Pb2vmLZ+di61iUipohIKpW6
eevWrTelGK8qrF6zjnFmlpMnTx6Nx+NX7QZEZNE0TePSpUs/PHHixJHTp09/yzCMJUc14k5EJjN/
B0BdNBp9/d69e0t+v38fgEsAYJrmNgC+/v7+1/r6+sZbWlr8uq5rmqZ5KgaglAIAENHPmLlw4cKF
K8eOHWvSdd0L4AP7vYuLi4vxeDynadrjc+fOvQYAjirFfiOAzvHx8XeJCPX19QEL2KAFMgkAhw8f
/tX27dv/Gg6HD7hcrpCIvKGqxIEOALhx48YAAGzdunWb5ZVh6zgvIt1KqcHm5uZdLpdrQkReUUqd
clQpAV5iZvT29t7RNI3C4fBeAOMWOIgIKaWuAuhayR9V4cqLPPh2KpUaUEpROp0urF+/fq+I/MNa
PYhI7GBsz1XGASICM3sA1E1OTr7j9/v13bt3BxwOh4OZ37PfZzv+V+5XzAEi2gMAd+/eHdM0jdra
2pqsS9FVZVEV4r/LNE3MzMykcrkch8PhRgAmEX25moerQcIOwzCWYrFYSkQkFAptATCjlFpYUw/Y
quz+TCbzRSqVypumKX6/fyeA6/avY0UA7N/4lQS0xp54PD4UCoVcSinyer2NAN62V8knfTVXHQKl
FEQEzPw2ER0AoK9k8tjYWMzr9WpNTU3rranfi8hv7XgBsIj8mYh+ihUXVlNqtxDRvUQi8a/x8fEh
TdOKwMXpdHouX77cJyKSzWb5+PHjXzNN07A/bxhGvq2t7WWv15snoi3lkJABIBAIdPv9/gMWcCKi
5bm5uU+mp6f/lsvlzLNnz/4gHA4fERGnzUtCRLqu6xCRobKyQCk1wcw7NU17TtM0sgBkALzlcrk2
FgoFdrvd2oYNG/boun4TwI8A+O2hB1AA8GlZAEQERHQbwO0V88F0Ov0BANTW1jrdbncDgHeIaPAp
oaSyANgYXzTsEBEFIDA1NXUrGAy6ampqnLquuwAM2ECv9KRUpQ4QkQHgKwAwODh4WylFe/fubbYM
f/wk0FUvRET0dWbG8PDwo6WlJaOlpSVirXKuFK1ZMgBbdevOZDIxt9utkslkLhQKRQB8VsrqywJg
q247UqnUHaUU1dXVuT0ezwsA+kt+XznuN00zBKAuHo+PORwOam1tDei67i5qwDUHICJhAJiampp2
Op3a5s2b/ZZnRp4JACJqB4BoNPpARKSxsbHeAnZnTQEUCUhEhwqFQmZ4eHguk8kYDQ0NuwEsKKWW
1xSATdsdSaVSg8Fg0JlIJPKBQKCrKEJL3e45SgVgldNNDx8+fNfn8+nBYNDtdDo3iMj7paZgWRwg
oh0AMDk5OaFpGnV0dDRYRj8ra2tXjghlZszMzKTy+bzZ1NTUaM0/eFYA9hmGURgdHZ03DEM2bdq0
DUBS07T4swJwMJvNjk9MTGSYWQKBwA4At1YrQssGYHv5/mQyeT0UCjmz2Sz7fL4WAH1PE6FVywKl
FJh5AwCMjo4OezweRyQSqSMiiMiVsvsLpewBiOhFSwOMmaYpHR0dxS7XoIg41gyALbdfzOfz2ceP
H+fn5+dz9fX1uwE8UEpxUbiulQeKCNqz2exkOp3O+3w+R01NzVYAn9uV81qR8D8A0un0F/l8nmtr
a10+n28LgI/KqYClklCJiA4gND09/fm6dev01tbWkLVreq+iJtdqRaiIdALAtWvXYsvLy2ZnZ+c+
Kzs+qQSAYzX5r5QCER1lZgwNDcWz2Sw3NDQcKgqQJ0nwqnnAVlyOLCwsDOu6rjo7Ozd6vd7nAfyl
kviXkgUOAC8kEokRAGhvb39e0zQAuIIKh7K23v8vDJsA+GZnZyfu37+/FIlEvmGtvL9SAI6i+5j5
OSJqBZAnoo9WxHYnAMRisftdXV3BYDB4kJn/aOdIpa3WKyv6+AUR2WiT4RdN05RDhw51j4yMvGHd
016VFicz/05EJJlMftjT0/Pd/v7+X1sGfm7jwPKjR48+7O7u7jYMY0lE7lapwwpFRC8vLi7ePXXq
1KuBQMDd09Pz90KhkAPgtgDuAuC+efPm++fPn39J0zSPiJwpR4A+LQvqBgYG/tDY2OiJRqPzZ86c
OWhtsx9aRPuJiCAWi01GIpHTVgvuT8WfFNXodMvIyMibFy9e/H4sFnvLRgWNmZtFRGZnZ6/Ozc19
aoVmP6o5mHnQMIyCYRgZGwm/yswREcmJiKTT6RvW/OtP63RU4oFv2gyziBjMnLZ+qzEzm9bJP7FW
Q0SOisiXRSCmaeaKhkWEReTVIunKEZ7/a/wbSvCQsyTpHYcAAAAASUVORK5CYII=")
   'png t))

(defvar big-macro-image
  (create-image
   (base64-decode-string
"iVBORw0KGgoAAAANSUhEUgAAACMAAAAwCAYAAACbm8NsAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A
/wD/oL2nkwAAAAlwSFlzAAAN1wAADdcBQiibeAAAAAd0SU1FB90FFQA4JZHI5TAAAAYWSURBVFjD
7ZldaBzXFcf/587szH4GWR9rp7acByk1OHFsMDGhtFUeRHESk9gRhUBwMcKtyWdDSsAhgUDAoR9p
oG2eXGjjGptEIPCLMUHWW9SiOlJsuV6tutEiCStayZJ3tZ+z2r3n5CEzRhZaWURhkwfdl2HvzJz7
u//7v2fuvQsAEJFmZs7KKoWZp0UEIkJYZ2Hmf0qNwsy/rvWeCQCVSkV8Pl+gv7//nWQyOW1Zllks
Fpf279//0IEDB34jIiAiAiDrgSGi++bm5gZ6enr+HolE/ACQy+Wc7u7u94PBYGBNGJFv2shkMovJ
ZDIdCATMxcXF8q5du3L4doW01uVEIpFuamryA8D8/HxJRKprvWSukFe01uxehZkFAH1LIKlWq8zM
IiKoVqv3VFWtGkUE30dRNcb8e4Ex66CMbAjmO1aGNjRMm57Z9MymZzY9s+mZTc/8wD0jPyTPqA2s
9jYOs0wZARAkImMDPpL1qltTGcMwqFAoFAGQiJi0DiMtA64uU5NqKbuygzWV8fl8KpVKpd2qHxOR
XksdZl4+vAcA6HVsabx3/cwcrKmMbdvG4ODgjPv7d/dQxFBKQUTambkA4IGFhYVxy7JWxmcAkRWd
aCGiBSIqrOkZn8+n0un05wB+JSK7iejOPREBM3vwmplfBpAAEAQAv98f0Vrf5RlmdgD8aHkbRLRD
RIIiUq2pDBHRli1brEuXLv3Drb7BzJ2etEQEpRSYuVNExonob+VyOSUiFQCIRqN7CoVCxfOMiKBS
qSwAeHRFcy+68YyayogI/H6/OTg4ON3X1/e2u8XtE5EbIvK2iPxBROaJqE9rvTOZTH7MzGWllA/A
l5FI5BE3lrgXyWazkwAeFZFtRARmvh/AcSKqiAip1YzoOE7VMAwSEWlsbLQvXrx4/eTJk8+OjY19
lM/ng47jnCyVSi9ks9nZgYGB93p7e1/fsWNHZyAQeEBEXhWRZ4gI3d3dP02n02UACIfDvqGhoc9c
wAQzv0lEUwCgtRYi+ooAoFwuN1mWlerp6Xl9YmJioaOjY29/f/8X09PT+Wg0GmBmsSzLmJ2dLc7M
zDiRSMScnZ0tx2Kx4oULF57fu3fvy24jzymlPhERBaC8tLS0eOLEiaM7d+4MERHdvn3bOXr0aMee
PXt+aZpma7lc/jKTySS2b9/+JIB374I5d+7cb0OhkH348OEPqtVq6fLly++eOXNmOBqN+m3bNgDA
cZxqLperHjx4sO3QoUOvhUKhNhGZBfAzpVSCmT0vvUREH+bz+fipU6feymazS6FQyCyVSjoWi+Wu
Xr1aPH369GNdXV1/cQ8E/B5Ms2VZqfPnz79q27avq6vrAy8H5XK5G/F4/NOpqakpANTe3t7e1tb2
i3A4/KCrxu+VUm96Q+xOcSIiYea/EtErIsKJROJcPB6/ls/nS62trffv3r27s6mp6THXHbuI6P9w
z2daRUTOnj37Qm9v72sislSpVFqY+U8iMrfKmc8kM/+Rmf33ysbubBtZJUaOmf/FzMp73ltChLwD
Hdu2TQA+Igorpd4A8IbWOsrMBgAyDGNJKTXvJTtPjdXSg6vQZQCPMPMWrXVARMQwDCil5pRS2oMh
om9giOhBAJicnMwEAgGfW3cnUxqGMVej53o1kGVA4qYEKKXSANKrxOC7FldKqU6tNWKxWDYcDvvc
hx4CMHKv78p6vz/ruedJ1JXNZq80Nzdbw8PDt7XWDhE9VfcFOTPfB2D7zMzMFw0NDVZjY6OVyWT+
S0RddYchog4AGB0djYVCIbOlpcWfSCQGAPiZ+eF6b1Uer1QquH79+ldKKbJt2xgZGUm4xnuq3jA/
LxQKsdHR0UVy7T80NDRXKpVmADxdb5h9qVRqcNu2bX5xEwMR4datW/8B8BMvs9YLxozH4/9raGiw
vHXu1q1bA9euXfu3CxJcK5d857uD5ubmRsdx7qxZLcsyJiYmbrnT3q7nMN3ct2/fczdv3iyapkmm
adL4+Hj2yJEjz7ow6bqZhpnfFxEZGxv76NixY08cP378yStXrvzZ/QdkuK6nEsxsMHNCRKRYLE4W
i8UJF2SBmbfWczZ9DYe738nMYqrsAAAAAElFTkSuQmCC")
   'png t))

(defvar big-recur-before-image
  (create-image
   (base64-decode-string
"iVBORw0KGgoAAAANSUhEUgAAADcAAAAwCAYAAAC13uL+AAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A
/wD/oL2nkwAAAAlwSFlzAAAN1wAADdcBQiibeAAAAAd0SU1FB90FExUuNNhmTlIAAAAZdEVYdENv
bW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAK1klEQVRo3t1Za2wc1RX+zt3Zl+31Y22nSSkQ
Hg2tKFVBIVUj0Rc/qlRAW5UfBYpopdD+KKUppVJEKE0iKqAoAfqjVYXUf6U4VCFr7PXa67eT7MYm
8TOJnYfjRzBxYju22Xjt3Z1z+oN7zbA4abAdIFxpNLszd+bc8/ruOd8AV3gw8/xZRJSIEACICD4X
wyjoHJ8b5bSC14pIJTP/USunrnqPMbPSv/8rejDz1xyhenUqZxbOzOvFMZg5fKmQvaryTEQOiIic
OHEinkgkJrSOP73qQYSZfyEikkwmpzZt2rQ5FouFtPdOXmlwuWJJrZSCiPgA7ASAvXv3ViaTyUwo
FGqdmpo6TUQ3MvNTV9LA6krkmfGaiGwjoqKJiYlT4XC4q7S01D81NZXat29fAwAQ0WZm/iIRXRHv
qSvlNWZeQ0SPA0BjY2OdbduilKLi4mJfbW3tkXffffcIgACArVrRz75yZpFE9DwA38DAwNu1tbXH
CgoKvERELpdLud1uVV5eHhERJqJHmfn2K4GcaplDkvQi7wLwE2ZOhUKhuqKiIq9SypRd4vf73V1d
XWf6+vr26Udf1gah5QxPtdzoqMc/AKC9vb2ut7d3zO12uxyeJQDIyclxl5eXN8/MzEwR0beZ+REi
kuVET7WceUZEwsyPEdGtiURiPBKJtOXn53sWmp+bm+seHh6e7ujoMOCyVURylhNcljssVxLRdgDY
t29f5MyZMxf8fr/b3DaHWXxJSYl/165d8cnJyVMAVovIFqd3P3XlRASONmYLgKKxsbFjFRUVXaWl
pTmOMCMRoeyWh4iovr6+Tr9uMzNf837qyWfDczocbyGixwAgGo3W+Xw+i5mNt+aR1HEIM0teXp47
FoudGh4e7iIiBeAFM3ep6KmWCiIO6H8FAI4fP74/Ho8P+v1+S5vfRNmHwtJ40OVyKaUUKisro+l0
2iaih0TkbkeV8+koZxRj5h8B+EEqlZoLh8Mtubm5bqUUaa3kYt7W0C+BQMDb1dU12tvb26RD9rls
GZ+KctqyOwDg4MGDkd7e3rGcnBz35RjF/M5kMhwMBn2vv/56QzKZHCeiO5l5o1bU9Ykqp5tMSwt/
gohuSiQSZ0KhUFtJSYk/nU7bWmlxICWZQ4fl/H0REZfLRZOTk3PxeLxOK72dmfOIyF5s7qnFeoyI
Msy8goi26Kq/dmZmJu0IR5NX8wiZ9bxRFqRHYWGht6ampmt8fHwAwCoAW0zufaLK6fNfAARHRkYO
RyKRnkAg4MUHG5kBj/nzRcb8Tbfb7UqlUnZ1dXXYdA0i8tXFVi3WYvY1DdN3ANjIzOndu3eH5+bm
bJfLlbkYgHyMSkdVVVUdW7du3f6bb755vYi8AOA+AIqZ+eN4kRaRb6SUEmaOE9E3k8nk+cHBwWMe
j8e7VMXMsG3b9nq9/uuuu+5W7bF7lVIVxrDLrpyjogAz/4yI/qNvvQdgGIB/uZQDwDqUv6wR+RAR
rdUb/6Jz8HKUzGfmfgeTNS0iK3XuWCJiMbPFzJbzv/l9sf+Oax4dIc99IIJFRDZ9EjTd0yIiExMT
Q++8885hLf1vy0wufUFE5phZjh492myMyMx5y04oOZisUmZOiYi89tprf3/11Vdf0lYVEfnGUgQ7
n2Pmf4uIDA4Otm/cuPEPo6OjfVrBVz4OY6YuR6gjxl8hIvfQ0FBHa2vrUCwWG+7r62vR815eSrlk
nhORu4nowUwmY1dWVtaJiESj0aiuYx9n5rWObmR59jlmvouIHrBtG7W1tU0iIitXrswJhULN6XR6
hoi+w8z3LbVkEpHnAeDo0aON3d3dZ66//vr8WCw22N/fH9dTtjma46Up53jBDgA4fPhw3YEDB4YL
Cwt9lmWpkydPnm9ra6vRc19mZi8R2ZdjWYfhXPr8KyJaOzMzM15WVtZUVFTks21bCgoKvGVlZdF0
Op0goh86jEiLUk4vznzEeJCI7pydnZ188803W1asWJFjyqbi4mJfOBw+NDk5OQLgBgBPOHu3ywx7
m5kDRLQVAFpbW+snJiZmLctSpnIZGRl579ChQ4aS+Kujj1yc54iIbdv2aJoOra2tdePj40mXy6UM
ini9XmtmZia9d+/eqH7mTyKy8nKpOocBngKwanx8fCASiXQWFRV5Tc2p2yJPTU1N+/T09CiAW5j5
yf9Xd6rLqB+3A7h2bGysv6qqqj0vL8/jKHYBAAUFBd5wOHx4ZGSkG4CfmXfqOa5LhaaDmf4KEW0G
gEgkEs5kMux2u11OKsLn81nnzp2baW5ujuhr20Tk2ksZUV0KlkXkJiJ6EgCamprqUqmU7fF4XKbg
dSxS/H6/FY1GG3WYPcDM64jIvhhs61LKhP2LADA4OHgwFosNBgIBj4iIkwkTESkpKfFHo9Gjo6Oj
RwDkiMgzRvmFFFSXIlc1vLsGBgbaampq+goLC33ZXjMlV15enjsejw8dOXLE5MVLl8o9rRwz871K
qXvS6fRceXl5vdfrNR6jrLAlZhbLslRFRUWNfn6jNqIsJENlh4mu3YSZv09E99i2nYlEIs0FBQVe
27Y/wkq9b+D3R2lpac6ePXtaZmdnpwGsZ+aHFgobs3fqZ7cBQE9PT2NfX99YXl6e2whxeG3ee7m5
ue7u7u7R3t7eFgdtvyChpLLjXwtVxvKdnZ213d3do4bwMaywkwsxC7AsS42MjCT2799fpW+/JCLu
7KR3LPZ3AG5PJBKjZWVlLcXFxT4TFdob4pRBRFBKUW5urrVnz56mZDI5DeB7IvLzhcBFORljc1NE
fg3g64lEYqy8vDxeXFzsN51zNu+ouUjTqnBhYaE3HA53TkxMDBJRqYhszdpezFegfADbAaC5ubk6
mUxmlFKGgsjmOD909vl81unTp6fb2tqiZuN37JUfVc5RGPsAPAsAsVgsev78+fn9xhnbjrPzGrnd
btfc3Jzd0tJiuJDfM/ONC0D/s0QUOHv27PH6+vreQCDgYeZ5Gc5cdcoCgEwmw8XFxf6qqqp2/SHz
GgCb9RzL6KIW6MpfJKLg2bNnj7311lsdOtdYCyAHqTO/3uw8DAaDvqqqqqNDQ0Ptus97Iat+XEtE
v2VmVFdX18zNzdmWZSlHjymOAwAkW75Siqanp1MNDQ2mOnqGmW8iosx8hDjI1Qwz30pEvxERNDQ0
NDjin5zImOUByUJOISIKBoO+UCgUFZEMEd3PzN9yRMl2AOjv74/F4/HBYDDoM9B/sYbXyZYZecFg
0NfY2HhseHi4C4CHiP7srDuVMxGJaAcAOnnyZKyhoeGE+WC4mPrX4/G4enp6znV2dtbrd/9TG/LH
RLQhnU4nysrKokbGIuSIy+VSXq/XCoVCEWZOA3hYRL47j8jMbD4Y3gPgB5lMZmb37t31wWDQZzZo
fcBxZF8zLfOH5gQCAXdFRUX8woULEwBuY+YniehpTeDWnz59+j3LstRFZDhlYYH3g5nF5/O5enp6
zvX09DTp9e6YR28HtO8EgHg8Xt3R0XF2xYoV/qV2vESEvr6+iebm5soNGzY8TEQvAsDY2NipXbt2
xZhZpqam5pahmZY33nijZfXq1bfl5+ffISKPEtGrBmJ/CeBftm1jYGDg7Uwmk1bLx8JQJpNJrVmz
Zr3L5VJaucHR0dGh5WTMUqlUatWqVTcEg8EvATgsImtNSD4CYCcRzQLwLIbyu4yRNAQtEXkBuJeR
LTNImtIEUycR3f8/BcYHroonLnUAAAAASUVORK5CYII=")
   'png t))

(defvar big-recur-after-image
  (create-image
   (base64-decode-string
"iVBORw0KGgoAAAANSUhEUgAAADcAAAAwCAYAAAC13uL+AAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A
/wD/oL2nkwAAAAlwSFlzAAAN1wAADdcBQiibeAAAAAd0SU1FB90FExUvCO4SA5QAAAAZdEVYdENv
bW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAMbklEQVRo3t1aa2xc13H+5uyLXD7El6gHKFZW
mzZG4RhBYwctitRBURcFYtRK0QJGm7QCbKQIKthOCiNxisRS4sSI7agIUv9o7Ar+4cK0aEh8i+Ry
Kb60kk1KfEhc0aQpcsldrpeP3eWS+74z/eFzhRuWokSLcuRc4ILLu3PuzHdmzsx35izhU7hEBAAU
ACYi/E5czGwC2/LZZ+4SETCz+fkrzNwhIo9t8ObvhAffFxFh5nkRKfzMAxIR0sC+KR9frAH+QH9v
+8yuNQ3AzsxLIiJjY2NeZhZmNph5z2d+/THzCyIikUhk6siRI9+ZmZkZ0t77709Dv7qLYXmIiJ4X
EXg8ng6bzUYtLS3efD5vENFTIvKlu51Y1E6Ho7nWROSHAByBQGCot7d3ura2tmR0dPQjv9/fo2Vf
Nm24WwB33HNEJMz8ABH9s4hwQ0NDV0VFRUE+n+eqqqrCxsbGgUwms6aUeoSZ/4aI+J73nIhAKWUC
/CUAjI2Nef1+/6LD4bABgM1mo0AgEB8cHOzUcq+KiLpbrGVHwelw+1sAj6TT6URra+uFkpISp1ji
rqqqqrC5uXlodXV1AcD9IvKv1gx7T4KzeO0EAHR0dJzy+XwLq6ur2XA4vG7ey8vL6ampqfipU6fq
9NCXmdmtlNpxgLQTSYQ+voSZjxLRL0UkHwgEPlBK2QFsli3IMIx8TU3NH9jtdieAXxDRd833mRP1
WwUnIjfWmohUi8g1AOV6Dc0BcADgm+jJAagREQXAIKIvENH4vVqwT4jlYuaHbyG/n5mt8vX3FKlm
ZpM/HhARyeVyab/f36ftHbMkGZvlNsd0iYh88MEHA5lMJm6dkHsFnE3/bRQRGR8f737qqae+G4vF
QhrgtzfyTf35sIhIMpmMP/fcc8/7fL4GLf/eTnpP3cl6U0oZzPxVInosl8tlPR7Pe0SE/v7+Di3z
Y2a26yxqLfQvAsDly5e9KysraY/HM7y+vh4F8BAz/+NvvRQQEUSEiOhVbWiH3+9f2r9/f3Fra+vV
cDg8TkQVAH6mh5hePgrg/tXV1fl33nnnwoEDB0ojkcj6wMBAq37vyyLi2InCrj5hOCrtmW8A+GIy
mYycOXPmYnV1tVtE4HA4VFdX1zlt7LPMfIiIDGYuJaIfAUBPT49Hv0uqqqrcLS0tYysrK9cB7BOR
fzfD/k7CU33CcOR0Og0ReQkAzp8/35VKpfLam1JaWuq6ePHi7PT09PvaY8f08OcBVIbD4Yne3t7J
4uJip04kYrfbldfr9egJ+R4zVyuljE/Vc2a4OJ3O40qpfZFIZLKjo+NKUVGRg5nFlCkpKXG+++67
nYZhZIjon0TkW0T0LAC0traeJSLYbDZzByFut9ve29v74ezs7GUApea6vJPwVNsMxxs1ioieMwwD
7e3t7fF4PJNKpfJra2tZ885ms8bo6GjEJMki8hoA58TERL/X671ORLDKJ5PJPDNLfX39WcMw0gCe
ZOYH7iRz0nbXmlKKmfl/ieiJTCaTuH79+rjL5XLJJhYwM9vtdkdtbe0XTEo1Ozt7JZfLZdQmHIuI
KJPJZA4cOPC5oqKiChHpU0p9RURsRGTcNXAiAiICMz9IRMP6cQrAhwCKNc3adCiAjImdiJxbRAzp
d9YAKNV6H1NKNe8k59zKe+csLGuVme/Xz+3MbNdNIbuI/MZtfXYzWWZWIlLBzCsWWjYuIs672mDV
AB4XEUkkEouBQGBE6/+fHZ68F0VEgsHg1Wg0Oq8BftvaLtyxhKK3NOa/PweAoaGhcydPnjwtIjkA
R5j5oU+ifOPkichBIvoeALS1tXV0dna2aJEXDcNwEpFsJ7mo2039zPwdIvpcLBaba2pquhQKhdaG
h4e7tFG/usMtlEkKfgFATU5O+nw+35zP57seCoWuElEZEb203dKgbsdrIlJORN8HgP7+fm82mzUq
KioK2tvbB1OpVFwp9TAz/z0R8XbTtk5UzMxfJqLDhmHkGhsbz1VXV7sNw5Du7u4eLfOMiPz+dloS
6nZaByJyDEBVKBS60traerWsrMzldDptwWAwceHChXY9oz8XEfd2i64pbzaVhoeHuyYmJpYdDocq
Ly8v6OnpmZ6cnDwPgETkFbNk3BE4S8E+SERHAaClpaWztLTUadao3bt3u5ubm4ej0WgAwEFzi2Pu
2bZBCp4A8PD6+vpKU1PThcrKygITwO7duwsbGhq6DcPIEtHjzPyn5tq7VZSomym11JRXAGBqaso3
OjoaLiwstFt33EopdHV1ndWyLzBzsVJKbhU61lYggJcA4OLFi52xWCztcrlu6LDb7WpmZiY+MjLS
rb32yu2uPXULA/6KiP4umUzG33jjjdZoNJoJh8PrkUgkGYlEkuFweD2RSOTeeuut0cnJSR8RFRHR
y9aQ3qr3oifyP4iodmFhYfy1117rT6fTxkcffXRDRyQSSSaTyfzrr7/eFY1G5wH8GTN/43YA0i3A
XQDw5UQisRQOh0M2m82+mZxhGPmSkpLSvXv3HtTj/lgpNX4zVmE+F5E9APwAyhcXFwOxWCxus9ls
m63LXC6Xq66u3ltWVrZHRIJKqRorc7otcCZ/FJFvisibAISIYgDCAFw3adXlARSKSK02ppOIHtWb
Wdli8n4tIk/qMfMAkuamdhM7MwB2i0ilzik/IqLjt03LzFAxDMPOzLMWCjQmIrtExCYi7g13oZ6U
Vzd0v75qORzZjO384Ybu19v6e/cmt1NElJX6MXOMmfdue9cgIsdFREKhkH95eXlGv+zoFvJ7RUSy
2WxyYmKiX+sf3aw5ZHnmMbtf6XQ6psd8cQsdXxcRiUajofn5+bHboX5qEwpUA+D7ANDe3t7R1tbW
bGY0Zi62esM8+hWR/wKA6enpoRMnTpxJJBIRAA8w85PW5GJJ/V8jor9Mp9PrJ0+ebBsZGenVOn61
4TjMCu4EAAwODp578803T4uIoanf/TejfsoCzGzgHAdgn5ubuzw4OBi8dOlScGFhYZyI3AB+vOFc
wGDmPyeir+dyuZzH47kgIhgYGOjQ7/+pYRi0Ser/CQCMjIx4FxcXU16vdySZTMZ1JvyHTUL4WSKq
jcVic52dnVcWFhbWrl27NqDFTmjZ/3fOpyz7LIOZH1JKHTEMgxsbG7vcbrfdMAxua2vr1FTsGRF5
YAOzOAEAo6OjnqtXr0b27dtX1NTUNBqJRCaIaDcR/XRD9+tbRPRgIpEI1dXVna+pqSmen59P+Hy+
NrOOfVw+FfThyB4iOg4AXq+3PZlM5ouKihwNDQ296XQ6QUR/zcyPm/0WK0BFRFZDfwIAfr+/Z3Jy
cqmwsNC+a9cu19DQUHBqauq8hYpZmcWXUqnU0unTp3179uxxiwicTqfN6/V2W7pfv6cnz01ExwCg
r6+vy6x31dXV7paWlpFYLDYL4ICIPG1xwA8AFAeDwbG+vr4Py8rKXAUFBfZQKJQYHh42dRw3J9xa
FpTllweHATyaSqXi9fX1PaWlpS59ekPl5eUFdXV1nfl8fp2IDjPzI3rMf2oy3RGPxzNms6ekpMTZ
09Pz4fT09HsAXET0M638BQB7FhYW/B6PZ7yoqMhhdr+YWTo7O02e+iIzl4rIfUR0VJ/QtrtcLpvN
ZlMAUFlZWVhXV+dLJBLzen0/bW07bkwor2gK1L64uJi02WxkKlVKUSgUWhsaGvKaRjLzMaVU9dLS
0rTH47laUlLiNAzjxn6rvLy84PTp012GYWQBPMHM/wLg3/TZnUcpRUop0r80kqKiIkd/f/90IBAY
AVBIRC+IyKsAcO3atb6RkZFwQUGB3bTJLCPd3d0eC/Vz6xptEn8FEXmGiA4tLS1Nv/322+8rpWhl
ZSUdj8cz5s3MOHXqlC8ajc4B+Asi+qFhGPm2trbW5eXl9Pr6es4qn0ql8oODg+FLly51auW/BlA4
MTHRf/bs2SkRgVV+bW0tl8lkjPr6+rZ8Pp8F8DQRHU4mk8v19fXnRATRaPSGTbFYLJPP57m5ufnK
3NzciN7zHfuNfMDMxUQ0AuBQJBKZXlxcDDqdzs16FpTNZtN79+49WFlZWQsAmUwmNTU19b7L5SrY
bItsGAY7nU7nfffd9yemwpmZmeF0Op0yw2tj9yudTqcPHTr0oNvt3gUAsVgsHAwGJ29iE/L5fH7X
rl1V+/fv/yMAayLyeaVU0ARXCeAMgM/r9pnjJhTLpEFZEclqWxSAwi3kze5XSmMXIiq8BWEnAGld
x4SIHFvQvhvzqCc3BuBRpVQAAP4Ph8S7KawnaNMAAAAASUVORK5CYII=")
   'png t))

(defvar big-lambda-toggled-on t)
(make-variable-buffer-local 'big-lambda-toggled-on)

;; 0: lambda
;; 1: loop
;; 2: function definition
;; 3: macro definition
;; 4: recur-before
;; 5: recur-after
(defvar big-image-name-list '(big-lambda-image big-loop-image big-function-image big-macro-image big-recur-before-image big-recur-after-image))
(defvar big-image-list (map 'list #'symbol-value big-image-name-list))

(dolist (big-image-name big-image-name-list)
  (unintern big-image-name nil))

(defvar big-lambda-font-lock-keywords
  '(
    ("[lambdfnopeucr\\-]+"
     (0 (prog1 nil
	  (big-x-remove-region (match-beginning 0) (match-end 0)))))

    ;; lambda
    ("(\\(lambda\\)\\>"
     (0 (prog1 nil
	  (if (not (equal (symbol-name major-mode) "clojure-mode"))
	      (big-x-region (match-beginning 1) (match-end 1) 0)))))
    ("(\\(fn\\)\\>"
     (0 (prog1 nil
	  (if (equal (symbol-name major-mode) "clojure-mode")
	      (big-x-region (match-beginning 1) (match-end 1) 0)))))

    ;; loop
    ("(\\(loop\\)\\>"
     (0 (prog1 nil
	  (if (not (equal (symbol-name major-mode) "clojure-mode"))
	      (big-x-region (match-beginning 1) (match-end 1) 1)
	    (big-x-region (match-beginning 1) (match-end 1) 4)))))

    ;; functions
    ("(\\(defun\\)\\>"
     (0 (prog1 nil
	  (if (not (equal (symbol-name major-mode) "clojure-mode"))
	      (big-x-region (match-beginning 1) (match-end 1) 2)))))
    ("(\\(defn-?\\)\\>"
     (0 (prog1 nil
	  (if (equal (symbol-name major-mode) "clojure-mode")
	      (big-x-region (match-beginning 1) (match-end 1) 2)))))

    ;; macros
    ("(\\(defmacro\\)\\>"
     (0 (prog1 nil
	  (big-x-region (match-beginning 1) (match-end 1) 3))))

    ;; recur
    ("(\\(recur\\)\\>"
     (0 (prog1 nil
	  (if (equal (symbol-name major-mode) "clojure-mode")
	      (big-x-region (match-beginning 1) (match-end 1) 5)))))
    
    ))

(defmacro with-all-due-respect (body)
  `(save-restriction
    (widen)
    (let ((modified-p (buffer-modified-p))
	  (font-lock-keywords big-lambda-font-lock-keywords))
      ,body
      (set-buffer-modified-p modified-p))))

(defun big-x-remove-region (beg end)
  (let (pos)
    (dolist (big-x-image big-image-list)
      (while (setq pos (text-property-any beg end 'display big-x-image))
	(remove-text-properties
	 pos
	 (or (next-single-property-change pos 'display) end)
	 '(display))))))

(defun big-x-region (beg end big-image-index)
  (cl-labels ((comment-or-string? (property-symbol)
				  (let ((property-symbol-string (symbol-name property-symbol)))
				    (or (string= property-symbol-string "font-lock-comment-face")
					(string= property-symbol-string "font-lock-string-face"))))

	      (disallowed-location? (location)
				    (let ((face-properties (get-text-property location 'face)))
				      (cond ((consp face-properties) (cl-remove-if-not
								      #'(lambda (s) (and (symbolp s) (comment-or-string? s)))
								      face-properties))
					    ((symbolp face-properties) (comment-or-string? face-properties))
					    (t nil)))))
    (if (and big-lambda-toggled-on (not (disallowed-location? beg)))
	(put-text-property beg end 'display (nth big-image-index big-image-list)))))

(defgroup biglambda nil
  "Big Lambda"
  :version "33"
  :group 'faces
  :prefix "biglambda-")

(defcustom biglambda-mode-lighter " BigLambda"
  "Lighter displayed in mode line when `biglambda-mode' is enabled."
  :group 'biglambda
  :type 'string)

(define-minor-mode big-lambda-mode
  "Display big lambda."
  :lighter biglambda-mode-lighter
  (if big-lambda-mode
      (progn
	(with-all-due-respect
	 (font-lock-fontify-buffer))
	(font-lock-add-keywords nil big-lambda-font-lock-keywords))
    (progn
      (font-lock-remove-keywords nil big-lambda-font-lock-keywords)
      (with-all-due-respect
       (big-x-remove-region (point-min) (point-max))))))

(defun toggle-big-lambda ()
  (interactive)
  (if big-lambda-mode
      (progn
	(setq big-lambda-toggled-on (not big-lambda-toggled-on))
	(with-all-due-respect
	 (cond (big-lambda-toggled-on (font-lock-fontify-buffer))
	       ((not big-lambda-toggled-on) (big-x-remove-region (point-min) (point-max)))))
	(message "Big Lambdas Toggled."))))

(provide 'biglambda)
